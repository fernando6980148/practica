<?php
class Jugadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Jugador");
        $this->load->model("Equipo");
        $this->load->model("Posicion");
    }

    public function index()
    {
        $data["listadoJugadores"] = $this->Jugador->consultarConRelaciones();
        $this->load->view("header");
        $this->load->view("jugadores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_jug)
    {
        $this->Jugador->eliminar($id_jug);
        $this->session->set_flashdata("confirmacion", "Jugador eliminado exitosamente");
        redirect("jugadores/index");
    }

    public function nuevo()
    {
        $data["nombresEquipos"] = $this->Equipo->consultarNombresEquipos();
        $data["nombresPosiciones"] = $this->Posicion->consultarNombresPosiciones();
        $this->load->view("header");
        $this->load->view("jugadores/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarJugador()
    {
        $datosNuevoJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_equi" => $this->input->post("id_equi_equipo"),
            "fk_id_pos" => $this->input->post("id_pos_posicion")
        );
        $this->Jugador->insertar($datosNuevoJugador);
        $this->session->set_flashdata("confirmacion", "Jugador guardado exitosamente");
        redirect('jugadores/index');
    }

    public function editar($id_jug)
    {
        $data["jugadorEditar"] = $this->Jugador->obtenerPorIdConRelaciones($id_jug);
        $data["nombresEquipos"] = $this->Equipo->consultarNombresEquipos();
        $data["nombresPosiciones"] = $this->Posicion->consultarNombresPosiciones();
        $this->load->view("header");
        $this->load->view("jugadores/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarJugador()
    {
        $id_jug = $this->input->post("id_jug");
        $datosJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_equi" => $this->input->post("id_equi_equipo"),
            "fk_id_pos" => $this->input->post("id_pos_posicion")
        );
        
        $this->Jugador->actualizar($id_jug, $datosJugador);
        $this->session->set_flashdata("confirmacion", "Jugador actualizado exitosamente");
        redirect('jugadores/index');
    }
}
?>
