<?php
// Crear clase Posiciones como controlador
class Posiciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Carga del modelo dentro del controlador
        $this->load->model("Posicion");
    }

    // Función para renderizar una vista index
    public function index()
    {
        $data["listadoPosiciones"] = $this->Posicion->consultarTodos(); // Array asociativo "Data"
        $this->load->view("header");
        $this->load->view("posiciones/index", $data);
        $this->load->view("footer");
    }

    // Eliminación recibiendo el id por GET
    public function borrar($id_pos)
    {
        // Corrección: Debe ser $this->Posicion en lugar de $this->Jugador
        $this->Posicion->eliminar($id_pos);
        $this->session->set_flashdata("confirmacion", "Posición eliminada exitosamente");
        redirect("posiciones/index");
    }

    // Renderización del formulario nuevo
    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("posiciones/nuevo");
        $this->load->view("footer");
    }

    // Capturando datos e insertando
    public function guardarPosicion()
    {
        $datosNuevaPosicion = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos")
        );
        $this->Posicion->insertar($datosNuevaPosicion);
        $this->session->set_flashdata("confirmacion", "Posición guardada exitosamente");
        redirect('posiciones/index');
    }

    // Renderizar el formulario de edición
    public function editar($id_pos)
    {
        $data["posicionEditar"] = $this->Posicion->obtenerPorId($id_pos);
        $this->load->view("header");
        $this->load->view("posiciones/editar", $data);
        $this->load->view("footer");
    }

    // Actualizar posición
    public function actualizarPosicion()
    {
        $id_pos = $this->input->post("id_pos");
        $datosPosicion = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos")
        );
        $this->Posicion->actualizar($id_pos, $datosPosicion);
        $this->session->set_flashdata("confirmacion", "Posición actualizada exitosamente");
        redirect('posiciones/index');
    }
} // Cierre de la clase
?>
