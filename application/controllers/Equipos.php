<?php
// Crear clase Equipos como controlador
class Equipos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Carga del modelo dentro del controlador
        $this->load->model("Equipo");
    }

    // Función para renderizar una vista
    public function index()
    {
        $data["listadoEquipos"] = $this->Equipo->consultarTodos(); // Array asociativo "Data"
        $this->load->view("header");
        $this->load->view("equipos/index", $data);
        $this->load->view("footer");
    }

    // Eliminación recibiendo el id por GET
    // Eliminación recibiendo el id por GET
    public function borrar($id_equi)
    {
        // Eliminar jugadores asociados al equipo
        $this->load->model("Jugador"); // Carga del modelo Jugador
        $this->Jugador->eliminar($id_equi); // Método para eliminar jugadores por id_equi

        // Luego eliminar el equipo
        $this->Equipo->eliminar($id_equi);
        $this->session->set_flashdata("confirmacion", "Equipo eliminado exitosamente");
        redirect("equipos/index");
    }

    // Renderización del formulario nuevo
    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("equipos/nuevo");
        $this->load->view("footer");
    }

    // Capturando datos e insertando
    public function guardarEquipo()
    {
        $datosNuevoEquipo = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi")
        );
        $this->Equipo->insertar($datosNuevoEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo guardado exitosamente");
        redirect('equipos/index');
    }

    // Renderizar el formulario de edición
    public function editar($id_equi)
    {
        $data["equipoEditar"] = $this->Equipo->obtenerPorId($id_equi);
        $this->load->view("header");
        $this->load->view("equipos/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarEquipo()
    {
        $id_equi = $this->input->post("id_equi");
        $datosEquipo = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi")
        );
        $this->Equipo->actualizar($id_equi, $datosEquipo);
        $this->session->set_flashdata("confirmacion", "Equipo actualizado exitosamente");
        redirect('equipos/index');
    }
} // Cierre de la clase
?>
