<?php
class Posicion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        $respuesta = $this->db->insert("posicion", $datos);
        return $respuesta;
    }

    function consultarTodos()
    {
        $posiciones = $this->db->get("posicion");
        if ($posiciones->num_rows() > 0) {
            return $posiciones->result();
        } else {
            return false;
        }
    }

    function eliminar($id_pos)
    {
        $this->db->where("id_pos", $id_pos);
        return $this->db->delete("posicion");
    }

    function obtenerPorId($id_pos)
    {
        $this->db->where("id_pos", $id_pos);
        $posicion = $this->db->get("posicion");
        if ($posicion->num_rows() > 0) {
            return $posicion->row();
        } else {
            return false;
        }
    }

    function actualizar($id_pos, $datos)
    {
        $this->db->where("id_pos", $id_pos);
        return $this->db->update("posicion", $datos);
    }

    // Nuevo método para consultar nombres de posiciones
    function consultarNombresPosiciones()
    {
        $this->db->select("id_pos, nombre_pos");
        $posiciones = $this->db->get("posicion");
        if ($posiciones->num_rows() > 0) {
            return $posiciones->result_array();
        } else {
            return array();
        }
    }
}
?>
