<?php
class Equipo extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        $respuesta = $this->db->insert("equipo", $datos);
        return $respuesta;
    }

    function consultarTodos()
    {
        $equipos = $this->db->get("equipo");
        if ($equipos->num_rows() > 0) {
            return $equipos->result();
        } else {
            return false;
        }
    }

    function eliminar($id_equi)
    {
        $this->db->where("id_equi", $id_equi);
        return $this->db->delete("equipo");
    }

    function obtenerPorId($id_equi)
    {
        $this->db->where("id_equi", $id_equi);
        $equipo = $this->db->get("equipo");
        if ($equipo->num_rows() > 0) {
            return $equipo->row();
        } else {
            return false;
        }
    }

    function actualizar($id_equi, $datos)
    {
        $this->db->where("id_equi", $id_equi);
        return $this->db->update("equipo", $datos);
    }

    // Nuevo método para consultar nombres de todos los equipos
    function consultarNombresEquipos()
    {
        $this->db->select("id_equi, nombre_equi");
        $equipos = $this->db->get("equipo");
        if ($equipos->num_rows() > 0) {
            return $equipos->result_array();
        } else {
            return array();
        }
    }
}
?>
