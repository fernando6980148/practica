<?php
class Jugador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevo jugador
    function insertar($datos)
    {
        return $this->db->insert("jugador", $datos);
    }

    // Consultar todos los jugadores con sus equipos y posiciones
    function consultarConRelaciones()
    {
        $this->db->select('jugador.*, equipo.nombre_equi as nombre_equipo, posicion.nombre_pos as nombre_posicion');
        $this->db->from('jugador');
        $this->db->join('equipo', 'equipo.id_equi = jugador.fk_id_equi', 'left');
        $this->db->join('posicion', 'posicion.id_pos = jugador.fk_id_pos', 'left');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    // Eliminar jugador por ID
    function eliminar($id_jug)
    {
        $this->db->where("id_jug", $id_jug);
        return $this->db->delete("jugador");
    }

    // Obtener jugador por ID con sus relaciones
    function obtenerPorIdConRelaciones($id_jug)
    {
        $this->db->select('jugador.*, equipo.nombre_equi as nombre_equipo, posicion.nombre_pos as nombre_posicion');
        $this->db->from('jugador');
        $this->db->join('equipo', 'equipo.id_equi = jugador.fk_id_equi', 'left');
        $this->db->join('posicion', 'posicion.id_pos = jugador.fk_id_pos', 'left');
        $this->db->where("id_jug", $id_jug);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    // Actualizar jugador por ID
    function actualizar($id_jug, $datos)
    {
        $this->db->where("id_jug", $id_jug);
        return $this->db->update("jugador", $datos);
    }
}
?>
