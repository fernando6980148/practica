<h1><i class="fas fa-users"></i> Nuevo Equipo</h1>
<br>
<form action="<?php echo site_url('equipos/guardarEquipo'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
  <div class="form-group">
    <label for="nombre_equi"><b>NOMBRE DEL EQUIPO</b></label>
    <input type="text" name="nombre_equi" id="nombre_equi" class="form-control" value="" required placeholder="Ingrese el nombre del equipo">
    <span id="errorNombre_Equipo" class="error"></span>
  </div>

  <div class="form-group">
    <label for="siglas_equi"><b>SIGLAS</b></label>
    <input type="text" name="siglas_equi" id="siglas_equi" class="form-control" value="" required placeholder="Ingrese las siglas del equipo">
    <span id="errorSiglas" class="error"></span>
  </div>

  <div class="form-group">
    <label for="fundacion_equi"><b>FUNDACION</b></label>
    <input type="number" name="fundacion_equi" id="fundacion_equi" class="form-control" value="" required placeholder="Ingrese el año de fundación del equipo">
    <span id="errorFundacion" class="error"></span>
  </div>

  <div class="form-group">
    <label for="region_equi"><b>REGION</b></label>
    <select name="region_equi" id="region_equi" class="form-control" required>
      <option value="">Seleccione la región</option>
      <option value="COSTA">COSTA</option>
      <option value="SIERRA">SIERRA</option>
      <option value="ORIENTE">ORIENTE</option>
    </select>
    <span id="errorRegion" class="error"></span>
  </div>

  <div class="form-group">
    <label for="numero_titulos_equi"><b>NUMERO DE TITULOS</b></label>
    <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" class="form-control" value="" required placeholder="Ingrese el número de títulos del equipo">
    <span id="errorTitulos" class="error"></span>
  </div>

  <!-- Botones -->
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button>&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>&nbsp; CANCELAR</a>
      <br><br>
    </div>
  </div>
</form>

<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>

<script>
  function validarFormulario() {
    var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
    var regexNumero = /^\d{1,10}$/; // Expresión regular para validar hasta 10 números
    var nombre_equi = document.getElementById("nombre_equi").value;
    var siglas_equi = document.getElementById("siglas_equi").value;
    var fundacion_equi = document.getElementById("fundacion_equi").value;
    var region_equi = document.getElementById("region_equi").value;
    var numero_titulos_equi = document.getElementById("numero_titulos_equi").value;
    var error = false;

    if (!regex.test(nombre_equi)) {
      document.getElementById("errorNombre_Equipo").innerHTML = "Por favor, ingrese solo letras en el campo Nombre del Equipo.";
      error = true;
    } else {
      document.getElementById("errorNombre_Equipo").innerHTML = "";
    }

    if (!regex.test(siglas_equi)) {
      document.getElementById("errorSiglas").innerHTML = "Por favor, ingrese solo letras en el campo Siglas.";
      error = true;
    } else {
      document.getElementById("errorSiglas").innerHTML = "";
    }

    if (!regexNumero.test(fundacion_equi)) {
      document.getElementById("errorFundacion").innerHTML = "Por favor, ingrese hasta 10 números en el campo Fundación.";
      error = true;
    } else {
      document.getElementById("errorFundacion").innerHTML = "";
    }

    if (region_equi === "") {
      document.getElementById("errorRegion").innerHTML = "Por favor, seleccione una región.";
      error = true;
    } else {
      document.getElementById("errorRegion").innerHTML = "";
    }

    if (!regexNumero.test(numero_titulos_equi)) {
      document.getElementById("errorTitulos").innerHTML = "Por favor, ingrese hasta 10 números en el campo Número de Títulos.";
      error = true;
    } else {
      document.getElementById("errorTitulos").innerHTML = "";
    }

    return !error;
  }
</script>
