
<h1> <i class="fas fa-users"></i>EQUIPOS</h1>

<!-- Agregar boton -->
<div class="row">
  <div class="col-md-12 text-end">   <!--text-end-> para poner el boton a la derecha-->
    <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i>
      Agregar Equipos
    </a>
  </div>


</div>

<?php if ($listadoEquipos): ?>
  <!--Tabla Estatica-->

    <table class="table table-bordered">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>SIGLAS</th>
                <th>FUNDACION</th>
                <th>REGION</th>
                <th>NUMERO TITULOS</th>

                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoEquipos as $equipo): ?>
                <tr>
                  <td><?php echo $equipo->id_equi; ?></td>
                  <td><?php echo $equipo->nombre_equi; ?></td>
                  <td><?php echo $equipo->siglas_equi; ?></td>
                  <td><?php echo $equipo->fundacion_equi; ?></td>
                  <td><?php echo $equipo->region_equi; ?></td>
                  <td><?php echo $equipo->numero_titulos_equi; ?></td>

                  <!--Boton eliminar-->
                  <td>
                    <a href="<?php echo site_url('equipos/editar/').$equipo->id_equi; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                      <a href="<?php echo site_url('equipos/borrar/').$equipo->id_equi; ?>" class="btn btn-danger">
                        Eliminar
                      </a>
                  </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


<!--Mensaje si no se encuentra ninguna agencia registrado-->
<?php else: ?>

  <div class="alert alert-danger">               <!--PAra enviar mensaje de alerta-->
      No se encontraron equipos registrados
  </div>
<?php endif; ?>
