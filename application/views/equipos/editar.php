<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Equipo</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <h1><i class="fas fa-users"></i> EDITAR EQUIPOS</h1>

  <form method="post" action="<?php echo site_url('equipos/actualizarEquipo'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
    <input type="hidden" name="id_equi" id="id_equi" value="<?php echo htmlspecialchars($equipoEditar->id_equi); ?>">

    <div class="form-group">
      <label for="nombre_equi"><b>NOMBRE DEL EQUIPO:</b></label>
      <input type="text" name="nombre_equi" id="nombre_equi" class="form-control" placeholder="Ingrese el nombre del equipo..." value="<?php echo htmlspecialchars($equipoEditar->nombre_equi); ?>" required>
      <span id="errorNombre_Equipo" class="error"></span>
    </div>

    <div class="form-group">
      <label for="siglas_equi"><b>SIGLAS:</b></label>
      <input type="text" name="siglas_equi" id="siglas_equi" class="form-control" placeholder="Ingrese las siglas del equipo..." value="<?php echo htmlspecialchars($equipoEditar->siglas_equi); ?>" required>
      <span id="errorSiglas" class="error"></span>
    </div>

    <div class="form-group">
      <label for="fundacion_equi"><b>FUNDACION:</b></label>
      <input type="number" name="fundacion_equi" id="fundacion_equi" class="form-control" placeholder="Ingrese el año de fundación del equipo..." value="<?php echo htmlspecialchars($equipoEditar->fundacion_equi); ?>" required>
      <span id="errorFundacion" class="error"></span>
    </div>

    <div class="form-group">
      <label for="region_equi"><b>REGION:</b></label>
      <select name="region_equi" id="region_equi" class="form-control" required>
        <option value="">Seleccione la región</option>
        <option value="COSTA" <?php if ($equipoEditar->region_equi == "COSTA") echo "selected"; ?>>COSTA</option>
        <option value="SIERRA" <?php if ($equipoEditar->region_equi == "SIERRA") echo "selected"; ?>>SIERRA</option>
        <option value="ORIENTE" <?php if ($equipoEditar->region_equi == "ORIENTE") echo "selected"; ?>>ORIENTE</option>
      </select>
      <span id="errorRegion" class="error"></span>
      <script>
        document.getElementById("region_equi").value = "<?php echo htmlspecialchars($equipoEditar->region_equi); ?>";
      </script>
    </div>

    <div class="form-group">
      <label for="numero_titulos_equi"><b>NUMERO DE TITULOS:</b></label>
      <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" class="form-control" placeholder="Ingrese el número de títulos del equipo..." value="<?php echo htmlspecialchars($equipoEditar->numero_titulos_equi); ?>" required>
      <span id="errorTitulos" class="error"></span>
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp; Actualizar </button> &nbsp; &nbsp;
        <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger"> <i class="fa fa-times fa-spin"></i> &nbsp; Cancelar</a>
      </div>
    </div>
  </form>

  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>

  <script>
    function validarFormulario() {
      var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
      var regexNumero = /^\d{1,10}$/;
      var nombre_equi = document.getElementById("nombre_equi").value;
      var siglas_equi = document.getElementById("siglas_equi").value;
      var fundacion_equi = document.getElementById("fundacion_equi").value;
      var region_equi = document.getElementById("region_equi").value;
      var numero_titulos_equi = document.getElementById("numero_titulos_equi").value;
      var error = false;

      if (!regex.test(nombre_equi)) {
        document.getElementById("errorNombre_Equipo").innerHTML = "Por favor, ingrese solo letras en el nombre del equipo.";
        error = true;
      } else {
        document.getElementById("errorNombre_Equipo").innerHTML = "";
      }

      if (!regex.test(siglas_equi)) {
        document.getElementById("errorSiglas").innerHTML = "Por favor, ingrese solo letras en las siglas del equipo.";
        error = true;
      } else {
        document.getElementById("errorSiglas").innerHTML = "";
      }

      if (!regexNumero.test(fundacion_equi)) {
        document.getElementById("errorFundacion").innerHTML = "Por favor, ingrese un año válido de fundación.";
        error = true;
      } else {
        document.getElementById("errorFundacion").innerHTML = "";
      }

      if (region_equi === "") {
        document.getElementById("errorRegion").innerHTML = "Por favor, seleccione una región.";
        error = true;
      } else {
        document.getElementById("errorRegion").innerHTML = "";
      }

      if (!regexNumero.test(numero_titulos_equi)) {
        document.getElementById("errorTitulos").innerHTML = "Por favor, ingrese un número válido de títulos.";
        error = true;
      } else {
        document.getElementById("errorTitulos").innerHTML = "";
      }

      return !error;
    }
  </script>
</body>
</html>
