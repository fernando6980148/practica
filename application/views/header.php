<!-- header.php -->
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Admin Panel</title>

  <!-- Meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Panel de administración del banco.">
  <meta name="keywords" content="admin, banco, administración, panel de control">

  <!-- Favicon -->
  <link rel="shortcut icon" href="images/favicon.png">
  <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer">

  <!-- SweetAlert2 CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/responsive.css">
  <link rel="stylesheet" href="css/custom.css">

  <style>
    /* Estilos personalizados para el sidebar y contenido */
    body {
      display: flex;
      min-height: 100vh; /* Asegura que el cuerpo ocupe al menos el 100% de la altura de la pantalla */
      margin: 0; /* Elimina el margen por defecto del body */
    }

    #sidebar {
      position: fixed;
      top: 0;
      left: 0;
      bottom: 0;
      width: 250px; /* Ancho deseado del sidebar */
      background-color: #343a40; /* Color de fondo del sidebar */
      padding: 20px;
      overflow-y: auto; /* Habilitar desplazamiento vertical si el contenido es largo */
      z-index: 100;
    }

    #sidebar ul {
      list-style: none;
      padding: 0;
    }

    #sidebar ul li {
      margin-bottom: 10px;
    }

    main {
      margin-left: 250px; /* Ajusta este valor al ancho del sidebar */
      padding: 20px;
      width: calc(100% - 250px); /* Calcula el ancho del contenido principal */
    }
  </style>

</head>
<body>
  <div id="sidebar">
    <div class="position-sticky">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('equipos/index'); ?>"><i class="fas fa-users"></i> EQUIPOS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('jugadores/index'); ?>"><i class="fas fa-user"></i> JUGADORES</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('posiciones/index'); ?>"><i class="fas fa-futbol"></i> POSICIONES</a>
        </li>
      </ul>
    </div>
  </div>

  <!-- Contenido principal -->
  <main>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="<?php echo site_url(); ?>">Admin Panel</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarContent">
          <form class="d-flex ms-auto my-2 my-lg-0">
            <input class="form-control me-2" type="search" placeholder="Buscar" aria-label="Buscar">
            <button class="btn btn-outline-light" type="submit">Buscar</button>
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid mt-4"> <!-- Utiliza container-fluid para que ocupe todo el ancho -->
      <!-- Contenido dinámico de la página -->
      <?php if ($this->session->flashdata('confirmacion')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.js"></script>
        <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function () {
            Swal.fire({
              title: "CONFIRMACIÓN",
              text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
              icon: "success"
            });
          });
        </script>
        <?php $this->session->set_flashdata('confirmacion', ''); ?>
      <?php endif; ?>
