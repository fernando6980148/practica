<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Jugador</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <h1><i class="fas fa-user"></i> EDITAR JUGADOR</h1>

  <form method="post" action="<?php echo site_url('jugadores/actualizarJugador'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
    <input type="hidden" name="id_jug" id="id_jug" value="<?php echo $jugadorEditar->id_jug; ?>">

    <div class="form-group">
      <label for="apellido_jug"><b>APELLIDO DEL JUGADOR:</b></label>
      <input type="text" name="apellido_jug" id="apellido_jug" class="form-control" placeholder="Ingrese el apellido del jugador..." value="<?php echo htmlspecialchars($jugadorEditar->apellido_jug); ?>" required>
      <span id="errorApellido" class="error"></span>
    </div>

    <div class="form-group">
      <label for="nombre_jug"><b>NOMBRE DEL JUGADOR:</b></label>
      <input type="text" name="nombre_jug" id="nombre_jug" class="form-control" placeholder="Ingrese el nombre del jugador..." value="<?php echo htmlspecialchars($jugadorEditar->nombre_jug); ?>" required>
      <span id="errorNombre" class="error"></span>
    </div>

    <div class="form-group">
      <label for="estatura_jug"><b>ESTATURA:</b></label>
      <input type="text" name="estatura_jug" id="estatura_jug" class="form-control" placeholder="Ingrese la estatura del jugador por (Ej: 1.75)" value="<?php echo htmlspecialchars($jugadorEditar->estatura_jug); ?>" required>
      <span id="errorEstatura" class="error"></span>
    </div>

    <div class="form-group">
      <label for="salario_jug"><b>SALARIO:</b></label>
      <input type="text" name="salario_jug" id="salario_jug" class="form-control" placeholder="Ingrese el salario del jugador por (Ej: 1200.50)" value="<?php echo htmlspecialchars($jugadorEditar->salario_jug); ?>" required>
      <span id="errorSalario" class="error"></span>
    </div>

    <div class="form-group">
      <label for="estado_jug"><b>ESTADO:</b></label>
      <select name="estado_jug" id="estado_jug" class="form-control" required>
        <option value="">Seleccione el estado del jugador</option>
        <option value="ACTIVO" <?php if ($jugadorEditar->estado_jug == "ACTIVO") echo "selected"; ?>>ACTIVO</option>
        <option value="INACTIVO" <?php if ($jugadorEditar->estado_jug == "INACTIVO") echo "selected"; ?>>INACTIVO</option>
      </select>
      <span id="errorEstado" class="error"></span>
    </div>

    <div class="form-group">
      <label for="id_equi_equipo"><b>Equipo:</b></label>
      <select name="id_equi_equipo" id="id_equi_equipo" class="form-control" required>
        <option value="">Seleccione el nombre del equipo</option>
        <?php foreach ($nombresEquipos as $equipo): ?>
          <option value="<?php echo $equipo['id_equi']; ?>" <?php echo ($equipo['id_equi'] == $jugadorEditar->fk_id_equi) ? 'selected' : ''; ?>><?php echo $equipo['nombre_equi']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label for="id_pos_posicion"><b>Posición:</b></label>
      <select name="id_pos_posicion" id="id_pos_posicion" class="form-control" required>
        <option value="">Seleccione la posición</option>
        <?php foreach ($nombresPosiciones as $posicion): ?>
          <option value="<?php echo $posicion['id_pos']; ?>" <?php echo ($posicion['id_pos'] == $jugadorEditar->fk_id_pos) ? 'selected' : ''; ?>><?php echo $posicion['nombre_pos']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-primary">
          <i class="fas fa-floppy-disk"></i> &nbsp; Actualizar
        </button> &nbsp; &nbsp;
        <a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-danger">
          <i class="fas fa-times"></i> &nbsp; Cancelar
        </a>
      </div>
    </div>
  </form>

  <script>
    function validarFormulario() {
      var regexTexto = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
      var regexDecimal = /^\d+(\.\d+)?$/; // Expresión regular para validar números decimales
      var apellido = document.getElementById("apellido_jug").value;
      var nombre = document.getElementById("nombre_jug").value;
      var estatura = document.getElementById("estatura_jug").value;
      var salario = document.getElementById("salario_jug").value;
      var estado = document.getElementById("estado_jug").value;
      var error = false;

      if (!regexTexto.test(apellido)) {
        document.getElementById("errorApellido").innerHTML = "Por favor, ingrese solo letras en el campo Apellido.";
        error = true;
      } else {
        document.getElementById("errorApellido").innerHTML = "";
      }

      if (!regexTexto.test(nombre)) {
        document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
        error = true;
      } else {
        document.getElementById("errorNombre").innerHTML = "";
      }

      if (!regexDecimal.test(estatura)) {
        document.getElementById("errorEstatura").innerHTML = "Por favor, ingrese un número válido en el campo Estatura (Ej. 1.75).";
        error = true;
      } else {
        document.getElementById("errorEstatura").innerHTML = "";
      }

      if (!regexDecimal.test(salario)) {
        document.getElementById("errorSalario").innerHTML = "Por favor, ingrese un número válido en el campo Salario (Ej. 1200.50).";
        error = true;
      } else {
        document.getElementById("errorSalario").innerHTML = "";
      }

      if (estado === "") {
        document.getElementById("errorEstado").innerHTML = "Por favor, seleccione un estado.";
        error = true;
      } else {
        document.getElementById("errorEstado").innerHTML = "";
      }

      return !error;
    }
  </script>
</body>
</html>
