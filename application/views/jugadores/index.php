<h1><i class="fas fa-user"></i>JUGADORES</h1>

<!-- Agregar botón  -->
<div class="row">
  <div class="col-md-12 text-end">
    <a href="<?php echo site_url('jugadores/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i>
      Agregar Jugador
    </a>
  </div>
</div>

<?php if ($listadoJugadores): ?>
  <!-- Tabla de jugadores -->
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>APELLIDO</th>
        <th>NOMBRE</th>
        <th>ESTATURA</th>
        <th>SALARIO</th>
        <th>ESTADO</th>
        <th>EQUIPO</th>
        <th>POSICIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoJugadores as $jugador): ?>
        <tr>
          <td><?php echo $jugador->id_jug; ?></td>
          <td><?php echo $jugador->apellido_jug; ?></td>
          <td><?php echo $jugador->nombre_jug; ?></td>
          <td><?php echo $jugador->estatura_jug; ?></td>
          <td><?php echo $jugador->salario_jug; ?></td>
          <td><?php echo $jugador->estado_jug; ?></td>
          <td><?php echo $jugador->nombre_equipo; ?></td>
          <td><?php echo $jugador->nombre_posicion; ?></td>
          <td>
            <a href="<?php echo site_url('jugadores/editar/') . $jugador->id_jug; ?>" class="btn btn-warning" title="Editar">
              <i class="fa fa-pen"></i>
            </a>
            <a href="<?php echo site_url('jugadores/borrar/') . $jugador->id_jug; ?>" class="btn btn-danger">
              Eliminar
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <!-- Mensaje si no hay jugadores registrados -->
  <div class="alert alert-danger">
    No se encontraron jugadores registrados.
  </div>
<?php endif; ?>
