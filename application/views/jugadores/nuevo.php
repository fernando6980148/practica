<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nuevo Jugador</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <h1><i class="fas fa-user"></i> NUEVO JUGADOR</h1>
  <br>
  <form action="<?php echo site_url('jugadores/guardarJugador'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
    <div class="form-group">
      <label for="apellido_jug"><b>APELLIDO DEL JUGADOR</b></label>
      <input type="text" name="apellido_jug" id="apellido_jug" class="form-control" value="<?php echo set_value('apellido_jug'); ?>" required placeholder="Ingrese el apellido del jugador">
      <span class="error"><?php echo form_error('apellido_jug'); ?></span>
    </div>

    <div class="form-group">
      <label for="nombre_jug"><b>NOMBRE DEL JUGADOR</b></label>
      <input type="text" name="nombre_jug" id="nombre_jug" class="form-control" value="<?php echo set_value('nombre_jug'); ?>" required placeholder="Ingrese el nombre del jugador">
      <span class="error"><?php echo form_error('nombre_jug'); ?></span>
    </div>

    <div class="form-group">
      <label for="estatura_jug"><b>ESTATURA</b></label>
      <input type="text" name="estatura_jug" id="estatura_jug" class="form-control" value="<?php echo set_value('estatura_jug'); ?>" required placeholder="Ingrese la estatura del jugador (Ej. 1.75)">
      <span class="error"><?php echo form_error('estatura_jug'); ?></span>
    </div>

    <div class="form-group">
      <label for="salario_jug"><b>SALARIO</b></label>
      <input type="text" name="salario_jug" id="salario_jug" class="form-control" value="<?php echo set_value('salario_jug'); ?>" required placeholder="Ingrese el salario del jugador (Ej. 1200.50)">
      <span class="error"><?php echo form_error('salario_jug'); ?></span>
    </div>

    <div class="form-group">
      <label for="estado_jug"><b>ESTADO</b></label>
      <select name="estado_jug" id="estado_jug" class="form-control" required>
        <option value="">Seleccione el estado</option>
        <option value="ACTIVO" <?php echo set_select('estado_jug', 'ACTIVO'); ?>>ACTIVO</option>
        <option value="INACTIVO" <?php echo set_select('estado_jug', 'INACTIVO'); ?>>INACTIVO</option>
      </select>
      <span class="error"><?php echo form_error('estado_jug'); ?></span>
    </div>

    <div class="form-group">
      <label for="id_equi_equipo"><b>Equipo</b></label>
      <select name="id_equi_equipo" id="id_equi_equipo" class="form-control" required>
        <option value="">Seleccione el nombre del equipo</option>
        <?php foreach ($nombresEquipos as $equipo): ?>
          <option value="<?php echo $equipo['id_equi']; ?>"><?php echo $equipo['nombre_equi']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label for="id_pos_posicion"><b>Posición</b></label>
      <select name="id_pos_posicion" id="id_pos_posicion" class="form-control" required>
        <option value="">Seleccione la posición</option>
        <?php foreach ($nombresPosiciones as $posicion): ?>
          <option value="<?php echo $posicion['id_pos']; ?>"><?php echo $posicion['nombre_pos']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <!-- Botones -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
          <i class="fas fa-check"></i>&nbsp;&nbsp; GUARDAR
        </button>&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-danger">
          <i class="fas fa-window-close"></i>&nbsp; CANCELAR
        </a>
        <br><br>
      </div>
    </div>
  </form>

  <script>
    function validarFormulario() {
      var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
      var regexDecimal = /^\d+(\.\d+)?$/; // Expresión regular para validar números decimales
      var apellido = document.getElementById("apellido_jug").value;
      var nombre = document.getElementById("nombre_jug").value;
      var estatura = document.getElementById("estatura_jug").value;
      var salario = document.getElementById("salario_jug").value;
      var estado = document.getElementById("estado_jug").value;
      var error = false;

      if (!regex.test(apellido)) {
        document.getElementById("errorApellido").innerHTML = "Por favor, ingrese solo letras en el campo Apellido.";
        error = true;
      } else {
        document.getElementById("errorApellido").innerHTML = "";
      }

      if (!regex.test(nombre)) {
        document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
        error = true;
      } else {
        document.getElementById("errorNombre").innerHTML = "";
      }

      if (!regexDecimal.test(estatura)) {
        document.getElementById("errorEstatura").innerHTML = "Por favor, ingrese un número válido en el campo Estatura (Ej. 1.75).";
        error = true;
      } else {
        document.getElementById("errorEstatura").innerHTML = "";
      }

      if (!regexDecimal.test(salario)) {
        document.getElementById("errorSalario").innerHTML = "Por favor, ingrese un número válido en el campo Salario (Ej. 1200.50).";
        error = true;
      } else {
        document.getElementById("errorSalario").innerHTML = "";
      }

      if (estado === "") {
        document.getElementById("errorEstado").innerHTML = "Por favor, seleccione un estado.";
        error = true;
      } else {
        document.getElementById("errorEstado").innerHTML = "";
      }

      return !error;
    }
  </script>
</body>
</html>
