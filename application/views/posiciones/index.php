<h1><i class="fas fa-futbol"></i> POSICIONES</h1>

<!-- Agregar botón -->
<div class="row">
  <div class="col-md-12 text-end"> <!-- text-end para poner el botón a la derecha -->
    <a href="<?php echo site_url('posiciones/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i> Agregar Posición
    </a>
  </div>
</div>

<?php if ($listadoPosiciones): ?>
  <!-- Tabla Estática -->
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPosiciones as $posicion): ?>
        <tr>
          <td><?php echo $posicion->id_pos; ?></td>
          <td><?php echo $posicion->nombre_pos; ?></td>
          <td><?php echo $posicion->descripcion_pos; ?></td>
          <!-- Botón eliminar -->
          <td>
            <a href="<?php echo site_url('posiciones/editar/' . $posicion->id_pos); ?>" class="btn btn-warning" title="Editar">
              <i class="fa fa-pen"></i>
            </a>
            <a href="<?php echo site_url('posiciones/borrar/' . $posicion->id_pos); ?>" class="btn btn-danger" title="Eliminar">
              <i class="fa fa-trash"></i> Eliminar
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger"> <!-- Para enviar mensaje de alerta -->
    No se encontraron posiciones registradas.
  </div>
<?php endif; ?>
