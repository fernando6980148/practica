<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Posición</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <h1><i class="fas fa-futbol"></i> Nueva Posición</h1>
  <br>
  <form action="<?php echo site_url('posiciones/guardarPosicion'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
    <div class="form-group">
      <label for="nombre_pos"><b>Nombre de la Posición</b></label>
      <input type="text" name="nombre_pos" id="nombre_pos" class="form-control" value="" required placeholder="Ingrese el nombre de la posición">
      <span id="errorNombre" class="error"></span>
    </div>

    <div class="form-group">
      <label for="descripcion_pos"><b>Descripción</b></label>
      <input type="text" name="descripcion_pos" id="descripcion_pos" class="form-control" value="" required placeholder="Ingrese la descripción">
      <span id="errorDescripcion" class="error"></span>
    </div>

    <!-- Botones -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
          <i class="fas fa-check"></i>&nbsp;&nbsp; Guardar
        </button>&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger">
          <i class="fas fa-window-close"></i>&nbsp; Cancelar
        </a>
        <br><br>
      </div>
    </div>
  </form>

  <script>
    function validarFormulario() {
      // Expresiones regulares para la validación
      var regexText = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/; // Solo letras y espacios
      var regexTextLong = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s\.,-]{5,}$/; // Texto más largo con puntuación

      // Obtener elementos del formulario
      var nombre = document.getElementById("nombre_pos").value;
      var descripcion = document.getElementById("descripcion_pos").value;
      var error = false;

      // Validar 'Nombre de la Posición'
      if (!regexText.test(nombre)) {
        document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo 'Nombre de la Posición'.";
        error = true;
      } else {
        document.getElementById("errorNombre").innerHTML = "";
      }

      // Validar 'Descripción'
      if (!regexTextLong.test(descripcion)) {
        document.getElementById("errorDescripcion").innerHTML = "Por favor, ingrese una descripción válida (mínimo 5 caracteres).";
        error = true;
      } else {
        document.getElementById("errorDescripcion").innerHTML = "";
      }

      // Devolver false si hay errores, de lo contrario, enviar el formulario
      return !error;
    }
  </script>
</body>
</html>
