<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editar Posición</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <h1><i class="fas fa-futbol"></i> Editar Posición</h1>

  <form method="post" action="<?php echo site_url('posiciones/actualizarPosicion'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
    <input type="hidden" name="id_pos" id="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">

    <div class="form-group">
      <label for="nombre_pos"><b>Nombre de la Posición:</b></label>
      <input type="text" name="nombre_pos" id="nombre_pos" class="form-control" placeholder="Ingrese el nombre de la posición..." value="<?php echo $posicionEditar->nombre_pos; ?>" required>
      <span id="errorNombre" class="error"></span>
    </div>

    <div class="form-group">
      <label for="descripcion_pos"><b>Descripción:</b></label>
      <input type="text" name="descripcion_pos" id="descripcion_pos" class="form-control" placeholder="Ingrese la descripción..." value="<?php echo $posicionEditar->descripcion_pos; ?>" required>
      <span id="errorDescripcion" class="error"></span>
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-primary">
          <i class="fas fa-floppy-disk"></i> &nbsp; Actualizar
        </button> &nbsp; &nbsp;
        <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger">
          <i class="fas fa-times"></i> &nbsp; Cancelar
        </a>
      </div>
    </div>
  </form>

  <script>
    function validarFormulario() {
      // Expresiones regulares para la validación
      var regexText = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/; // Solo letras y espacios
      var regexTextLong = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s\.,-]{5,}$/; // Texto más largo con puntuación

      // Obtener elementos del formulario
      var nombre = document.getElementById("nombre_pos").value;
      var descripcion = document.getElementById("descripcion_pos").value;
      var error = false;

      // Validar 'Nombre de la Posición'
      if (!regexText.test(nombre)) {
        document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo 'Nombre de la Posición'.";
        error = true;
      } else {
        document.getElementById("errorNombre").innerHTML = "";
      }

      // Validar 'Descripción'
      if (!regexTextLong.test(descripcion)) {
        document.getElementById("errorDescripcion").innerHTML = "Por favor, ingrese una descripción válida (mínimo 5 caracteres y solo numeros).";
        error = true;
      } else {
        document.getElementById("errorDescripcion").innerHTML = "";
      }

      // Devolver false si hay errores, de lo contrario, enviar el formulario
      return !error;
    }
  </script>
</body>
</html>
